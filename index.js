require('dotenv').config();
const axios = require('axios');
const { Configuration, OpenAIApi } = require("openai");
const Discord = require('discord.js');
const { joinVoiceChannel, createAudioPlayer, createAudioResource } = require('@discordjs/voice');




// OpenAI client configuration
const configuration = new Configuration({ apiKey: process.env.OPENAI_API_KEY });
const openai = new OpenAIApi(configuration);

// Discord client configuration
const client = new Discord.Client({ intents: 131071 });



// Audio player and connection
const audioPlayer = createAudioPlayer();
let connection = null;

async function downloadMP3(textToSpeak) {
    const url = 'https://readthat.chelsea486mhz.fr/generate';

	console.log('Downloading MP3 for the following text:');
	console.log(textToSpeak);

    try {
        const response = await axios.post(url, {
			text: textToSpeak
		}, {
			headers: {
				'Authorization': process.env.READTHAT_TOKEN,
				'Content-Type': 'application/x-www-form-urlencoded'
			},
		responseType: 'arraybuffer'
		});

        // Save the response as an MP3 file
        const filePath = './output.mp3';
		require('fs').writeFileSync(filePath, response.data);

        console.log('MP3 file downloaded successfully.');
        return filePath;
    } catch (error) {
        console.error('Error occurred while downloading the MP3 file:', error);
        throw error;
    }
}



// Message history
let messageHistory = '';

// Respond to prompts
async function runCompletion(gptprompt) {
	messageHistory += `${gptprompt}\n`;

	try {
		const response = await openai.createChatCompletion({
			model: "gpt-3.5-turbo",
			messages: [{role: "user", content: messageHistory}],
		});
  
		// Extract the generated text from the response
		console.log('Completion succeeded. Response:');
		console.log(response.data.choices[0].message);
		messageHistory += `mrvegetal: ${response.data.choices[0].message}\n`;

		return response.data.choices[0].message.content;
	}

	catch (error) {
		console.log('Error occurred:');
		console.error(error);
  
		// Handle specific error scenarios
		if (error.response) {
			console.log('API response status:');
			console.log(error.response.status);
			console.log('API response data:');
			console.log(error.response.data);
		}

		return null;
	}
}


// Pre-prompt
var preprompt = 'Tu es Mr. Vegetal, un bot Discord. Adopte la personnalité d\'un brocoli trompettiste anarcho-communiste. Limite la longueur de tes messages pour qu\'ils soient le plus court possible.';
runCompletion(preprompt);



// Set up Discord bot events
client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`);
});

client.on('interactionCreate', interaction => {
	console.log(interaction);
});

client.on("messageCreate", async message => {
	console.log('Received message. Message:');
	console.log(message);
	const voiceChannel = message.member?.voice.channel;

	// Ignore messages from the bot itself
	if (message.author.id === client.user.id) {
		console.log('Message was from myself. Ignoring');
		return;
	}

	// Handle ping
	else if (message.content.startsWith("ping")) {
		console.log('Message is a ping request. Ignoring');
		message.channel.send("pong");
	}

	// Handle terminal commands
	else if (message.content.startsWith("terminal: ")) {
		console.log('Message is a terminal interaction. Sending to terminal handler');
		//TODO
	}

	// Otherwise, send to ChatGPT
	else {
		console.log('Replying using ChatGPT');

		const prompt = `${message.author.username}: ${message.content}`;
		const response = await runCompletion(prompt);
		console.log('Got ChatGPT reply. Sending the following message:');
		console.log(response);

		// The user is in a text channel
		if (!voiceChannel) {
			console.log('User is not in a voice channel. Replying by text');

			message.channel.send(response);
		}

		// The user is in a voice channel
		else {
			console.log('User is in a voice channel. Replying by voice');

			try {
				connection = joinVoiceChannel({
					channelId: voiceChannel.id,
					guildId: voiceChannel.guild.id,
					adapterCreator: voiceChannel.guild.voiceAdapterCreator,
				});

				const audioPath = await downloadMP3(response)
				const audioResource = createAudioResource(audioPath);
	
				audioPlayer.play(audioResource);
				connection.subscribe(audioPlayer);
	
				console.log('Playing audio in the voice channel');
			}
			
			catch (error) {
				console.error('Error occurred while playing audio:', error);
				message.channel.send("An error occurred while playing audio.");
			}
		}
	}
});


// Log into Discord
client.login(process.env.CLIENT_TOKEN);